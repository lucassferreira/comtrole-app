/*
Padrão de nomeclatura das bases
Nome das Classes: camelCase sempre no plural
Campos: camelCase sempre no singular
Chaves estrangeiras: Nome da tabela camelCase + underscore + nome do campo camelCase na tabela de origem. Exemplo: tabelaCamelCase_nomeDoCampoNaTabelaDeOrigem
Tabelas de relações N:N: devem conter os nomes das tabelas + underscore Ex: clietes_hosts_filas
1 -  Os campos devem ser descretivios e não abreviados quando possível.
*/

-------------------------------------------------------------------------
--AQUI É O INICIO DA PARTE BÁSICA
-------------------------------------------------------------------------
/*
	ESSA TAABELA TEM A FUNÇÃO DE IDENTIFICAR OS CLIENTES NO REGISTROS
*/


--TABELA PARA A IDENTIFICAÇÃO DOS CLIENTES
DROP TABLE IF EXISTS customers;
CREATE TABLE customers (
    --ESTE CAMPO É 14 porque CNPJ tem 14 digitos e CPF 11
    id VARCHAR(14) NOT NULL PRIMARY KEY,
    name varchar(120),
    domain varchar(120)
);

--TABELA PARA IDENTIFICAÇÃO DOS MODULOS
DROP TABLE IF EXISTS modules;
CREATE TABLE modules (
    id SERIAL NOT NULL PRIMARY KEY,
    name varchar (120),
    description text
);

--TABELA PRA IDENTIFICACAO DOS HOSTS
DROP TABLE IF EXISTS hosts;
CREATE TABLE hosts (
    id SERIAL NOT NULL PRIMARY KEY,
    name varchar(120),
    ip inet,
    hash varchar(32)
);

--TABELAS QUE RELACIONA CLIENTES MODULOS E HOSTS
DROP TABLE IF EXISTS licenses;
CREATE TABLE licenses (
	id SERIAL NOT NULL PRIMARY KEY,
    customers_id varchar(14) REFERENCES customers (id) ON DELETE CASCADE,
    modules_id integer REFERENCES modules (id) ON DELETE CASCADE,
    hosts_id integer REFERENCES hosts (id) ON DELETE CASCADE
);



-- Visão das tabelas CUSTOMERS MODULES E HOSTS


DROP VIEW IF EXISTS view_licenses;

CREATE VIEW view_licenses AS (
	SELECT
		a2.name as cliente,
		a2.domain as dominio,
		CONCAT(a3.name, '.conf') as modulo,
		a3.description as decricao,
		a4.name as hostname,
		a4.ip,
		a4.hash as fs_uuid
	FROM 
		licenses as a1
	INNER JOIN
		customers a2 ON a1.customers_id = a2.id
	INNER JOIN
		modules a3 ON a1.modules_id = a3.id
	INNER JOIN
		hosts as a4 ON a1.hosts_id = a4.id
);


--Registros para teste

INSERT INTO public.customers (id, "name", "domain") VALUES('1', 'teste1', 'teste1@teste1');
INSERT INTO public.customers (id, "name", "domain") VALUES('2', 'teste2', 'teste2@teste2');
INSERT INTO public.customers (id, "name", "domain") VALUES('3', 'teste3', 'teste3@teste3');

INSERT INTO public.hosts ("name", ip, hash) VALUES('fs1', '172.16.0.1', md5('fs1') );
INSERT INTO public.hosts ("name", ip, hash) VALUES('fs2', '172.16.0.2', md5('fs2') );
INSERT INTO public.hosts ("name", ip, hash) VALUES('fs3', '172.16.0.3', md5('fs3') );

INSERT INTO public.modules ("name", description) VALUES('ivr', 'Modulo de uras dinamicas');
INSERT INTO public.modules ("name", description) VALUES('callcenter', 'Modulo callcenter');

INSERT INTO public.licenses (customers_id, modules_id, hosts_id) VALUES('1', 1, 1);
INSERT INTO public.licenses (customers_id, modules_id, hosts_id) VALUES('1', 2, 1);

INSERT INTO public.licenses (customers_id, modules_id, hosts_id) VALUES('2', 1, 2);
INSERT INTO public.licenses (customers_id, modules_id, hosts_id) VALUES('2', 2, 2);

INSERT INTO public.licenses (customers_id, modules_id, hosts_id) VALUES('3', 1, 3);
INSERT INTO public.licenses (customers_id, modules_id, hosts_id) VALUES('3', 2, 3);

-------------------------------------------------------------------------
--AQUI É O FIM DA PARTE BÁSICA
-------------------------------------------------------------------------