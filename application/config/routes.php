<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

#ROTAS CUSTOMERS
$route['customers'] = 'customers';
$route['customers/show/(:num)'] = 'customers/show/$1';
$route['customers/form_add'] = 'customers/form_add';
$route['customers/add'] = 'customers/add';
$route['customers/form_adit/(:num)'] = 'customers/form_edit/$1';
$route['customers/edit/(:num)'] = 'customers/edit/$1';
$route['customers/del/(:num)'] = 'customers/del/$1';


#ROTAS HOSTS
$route['hosts'] = 'hosts';
$route['hosts/show/(:num)'] = 'hosts/show/$1';
$route['hosts/form_add'] = 'hosts/form_add';
$route['hosts/add'] = 'hosts/add';
$route['hosts/form_adit/(:num)'] = 'hosts/form_edit/$1';
$route['hosts/edit/(:num)'] = 'hosts/edit/$1';
$route['hosts/del/(:num)'] = 'hosts/del/$1';

#ROTAS MODULES
$route['modules'] = 'modules';
$route['modules/show/(:num)'] = 'modules/show/$1';
$route['modules/form_add'] = 'modules/form_add';
$route['modules/add'] = 'modules/add';
$route['modules/form_adit/(:num)'] = 'modules/form_edit/$1';
$route['modules/edit/(:num)'] = 'modules/edit/$1';
$route['modules/del/(:num)'] = 'modules/del/$1';

#ROTAS MODULES
$route['licenses'] = 'licenses';
$route['licenses/show'] = 'licenses/show';
$route['licenses/form_add'] = 'licenses/form_add';
$route['licenses/add'] = 'licenses/add';
$route['licenses/form_adit/(:num)'] = 'licenses/form_edit/$1';
$route['licenses/edit/(:num)'] = 'licenses/edit/$1';
$route['licenses/del/(:num)'] = 'licenses/del/$1';

