<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hosts_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function getall() {
        $query = $this->db->get('hosts');
         return $query->result_array();
    }

    public function getid($id) {
        if($id != FALSE) {
            $query = $this->db->get_where('hosts', array('id' => $id));
            return $query->row_array();
        }
        else {
            return FALSE;
        }
    }

    public function add($data) {
        if($this->db->insert('hosts', $data) == TRUE)
            return 'Registro inserido com sucesso!';
        else
            return 'Erro ao inserir registro!';
    }

    public function edit($data) {
        $this->db->where('id', $data['id']);
        if($this->db->update('hosts', $data) == TRUE)
            return 'Registro atualizado com sucesso!';
        else
            return 'Erro ao atualizar o registro!';
    }

    public function del($id) {
        if($id != FALSE) {
            $this->db->where('id', $id);
            if($this->db->delete('hosts') == TRUE)
                return 'Registro removido com sucesso!';
            else
                return 'Erro ao remover o registro!';
            }
        else {
            return FALSE;
        }
    }

}

?>