<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Licenses_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }


    public function getall() {
        $this->db->select('licenses.*, customers.name as customers_name, hosts.name as hosts_name, modules.name as modules_name');
        $this->db->from('licenses');
        $this->db->join('customers', 'licenses.customers_id = customers.id');
        $this->db->join('hosts', 'licenses.hosts_id = hosts.id');
        $this->db->join('modules', 'licenses.modules_id = modules.id');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getid($id) {
        if($id != FALSE) {
            $this->db->select('licenses.*, customers.name as customers_name, hosts.name as hosts_name, modules.name as modules_name');
            $this->db->from('licenses');
            $this->db->join('customers', 'licenses.customers_id = customers.id');
            $this->db->join('hosts', 'licenses.hosts_id = hosts.id');
            $this->db->join('modules', 'licenses.modules_id = modules.id');
            $this->db->where('licenses.id', $id);
            $query = $this->db->get();
            return $query->result_array();
        }
        else {
            return FALSE;
        }
    }

    public function add($data) {
        if($this->db->insert('licenses', $data) == TRUE)
            return 'Registro inserido com sucesso!';
        else
            return 'Erro ao inserir registro!';
    }

    public function edit($data) {
        $this->db->where('id', $data['id']);
        if($this->db->update('licenses', $data) == TRUE)
            return 'Registro atualizado com sucesso!';
        else
            return 'Erro ao atualizar o registro!';
    }

    public function del($id) {
        if($id != FALSE) {
            $this->db->where('id', $id);
            if($this->db->delete('licenses') == TRUE)
                return 'Registro removido com sucesso!';
            else
                return 'Erro ao remover o registro!';
            }
        else {
            return FALSE;
        }
    }

}
?>