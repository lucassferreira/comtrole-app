<?php
$this->load->helper('form');

echo form_open('modules/add', 
    array(
        'method'    => 'POST'
    )
);


echo form_input(
    array(
        'name'          => 'name',
        'id'            => 'name',
        'placeholder'   => 'Nome',
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'description',
        'id'            => 'description',
        'placeholder'   => 'Descrição',
    )
);
echo '<br><br>';

echo form_submit('', 'Cadastrar');
echo form_close();

?>