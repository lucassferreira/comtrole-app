<?php
$this->load->helper('form');

echo form_open('customers/add', 
    array(
        'method'    => 'POST'
    )
);

echo form_input(
    array(
        'name'          => 'id',
        'id'            => 'id',
        'placeholder'   => 'CNPJ/CPF',
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'name',
        'id'            => 'name',
        'placeholder'   => 'Nome',
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'domain',
        'id'            => 'domain',
        'placeholder'   => 'Domínio',
    )
);
echo '<br><br>';

echo form_submit('', 'Cadastrar');
echo form_close();

?>