<?php
$this->load->helper('form');

echo form_open('customers/edit', 
    array(
        'method'    => 'POST'
    )
);

echo form_input(
    array(
        'name'          => 'id',
        'id'            => 'id',
        'placeholder'   => 'CNPJ/CPF',
        'value'         => $data['id'],
        'readonly'      => 'true'
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'name',
        'id'            => 'name',
        'placeholder'   => 'Nome',
        'value'         => $data['name'],
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'domain',
        'id'            => 'domain',
        'placeholder'   => 'Domínio',
        'value'         => $data['domain'],
    )
);
echo '<br><br>';

echo form_submit('', 'Cadastrar');
echo form_close();

?>