<?php
$this->load->helper('form');

echo form_open('hosts/edit', 
    array(
        'method'    => 'POST'
    )
);

echo form_input(
    array(
        'name'          => 'id',
        'id'            => 'id',
        'placeholder'   => 'id',
        'value'         => $data['id'],
        'readonly'      => 'true'
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'name',
        'id'            => 'name',
        'placeholder'   => 'Nome',
        'value'         => $data['name']
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'ip',
        'id'            => 'ip',
        'placeholder'   => 'IPv4',
        'value'         => $data['ip']
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'hash',
        'id'            => 'hash',
        'value'         => $data['hash'],
        'readonly'      => 'true'
    )
);
echo '<br><br>';
echo form_submit('', 'Cadastrar');
echo form_close();

?>