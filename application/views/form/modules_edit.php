<?php
$this->load->helper('form');

echo form_open('modules/edit', 
    array(
        'method'    => 'POST'
    )
);

echo form_input(
    array(
        'name'          => 'id',
        'id'            => 'id',
        'placeholder'   => 'id',
        'value'         => $data['id'],
        'readonly'      => 'true'
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'name',
        'id'            => 'name',
        'placeholder'   => 'Nome',
        'value'         => $data['name']
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'description',
        'id'            => 'description',
        'placeholder'   => 'Descrição',
        'value'         => $data['description']
    )
);
echo '<br><br>';

echo form_submit('', 'Cadastrar');
echo form_close();

?>