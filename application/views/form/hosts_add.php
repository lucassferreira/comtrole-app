<?php
$this->load->helper('form');

echo form_open('hosts/add', 
    array(
        'method'    => 'POST'
    )
);


echo form_input(
    array(
        'name'          => 'name',
        'id'            => 'name',
        'placeholder'   => 'Nome',
    )
);
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'ip',
        'id'            => 'ip',
        'placeholder'   => 'IPv4',
    )
);
echo '<br><br>';

echo form_submit('', 'Cadastrar');
echo form_close();

?>