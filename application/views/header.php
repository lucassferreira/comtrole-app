<!DOCTYPE html>
<html>
<br><br>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>
</head>
<div class="dropdown">
  <button class="dropbtn">ClIENTES</button>
  <div class="dropdown-content">
    <a href="/customers/">Listar</a>
    <a href="/customers/form_add">Adicionar</a>
  </div>
</div>
<div class="dropdown">
<button class="dropbtn">HOSTS</button>
  <div class="dropdown-content">
    <a href="/hosts/">Listar</a>
    <a href="/hosts/form_add">Adicionar</a>
  </div>
</div>
<div class="dropdown">
<button class="dropbtn">MODULES</button>
  <div class="dropdown-content">
    <a href="/modules/">Listar</a>
    <a href="/modules/form_add">Adicionar</a>
  </div>
</div>
<div class="dropdown">
<button class="dropbtn">LICENCAS</button>
  <div class="dropdown-content">
    <a href="/licenses/">Listar</a>
    <a href="/licenses/form_add">Adicionar</a>
  </div>
</div>
<br><br>
</html>