<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modules extends CI_Controller {

	public function index(){
		$this->load->model('modules_model');
		$modules = $this->modules_model->getall();
		$data['data'] = $modules;
		$this->load->view('header');
		$this->load->view('modules', $data);
	}

	public function show($id) {
		$this->load->model('modules_model');
		$modules = $this->modules_model->getid($id);
		$data['data'] = $modules;
		$this->load->view('header');
		$this->load->view('modules', $data);
	}

	public function form_add() {
		$this->load->view('header');
		$this->load->view('form/modules_add');
	}

	public function add() {
		$this->load->model('modules_model');
		$return = $this->modules_model->add($_POST);
		$this->load->view('header');
		print $return;
	}

	public function form_edit($id) {
		$this->load->model('modules_model');
		$modules = $this->modules_model->getid($id);
		$data['data'] = $modules;
		$this->load->view('header');
		$this->load->view('form/modules_edit', $data);
	}

	public function edit() {
		$this->load->model('modules_model');
		$return = $this->modules_model->edit($_POST);
		$this->load->view('header');
		print $return;
	}

	public function del($id) {
		$this->load->model('modules_model');
		$return = $this->modules_model->del($id);
		$this->load->view('header');
		print $return;
	}
}
