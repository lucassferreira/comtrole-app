<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

	public function index(){
		$this->load->model('customers_model');
		$customers = $this->customers_model->getall();
		$data['data'] = $customers;
		$this->load->view('header');
		$this->load->view('customers', $data);
	}

	public function show($id) {
		$this->load->model('customers_model');
		$customers = $this->customers_model->getid($id);
		$data['data'] = $customers;
		$this->load->view('header');
		$this->load->view('customers', $data);
	}

	public function form_add() {
		$this->load->view('header');
		$this->load->view('form/customers_add');
	}

	public function add() {
		$this->load->model('customers_model');
		$return = $this->customers_model->add($_POST);
		print $return;
		$this->load->view('header');
	}

	public function form_edit($id) {
		$this->load->model('customers_model');
		$customers = $this->customers_model->getid($id);
		$data['data'] = $customers;
		$this->load->view('header');
		$this->load->view('form/customers_edit', $data);
	}

	public function edit() {
		$this->load->model('customers_model');
		$return = $this->customers_model->edit($_POST);
		$this->load->view('header');
		print $return;
	}

	public function del($id) {
		$this->load->model('customers_model');
		$return = $this->customers_model->del($id);
		$this->load->view('header');
		print $return;
	}
}
