<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Licenses extends CI_Controller {

	public function index(){
		$this->load->model('licenses_model');
		$licenses = $this->licenses_model->getall();
		$data['data'] = $licenses;
		$this->load->view('header');
		$this->load->view('licenses', $data);
	}

	public function show($id) {
		$this->load->model('licenses_model');
		$licenses = $this->licenses_model->getid($id);
		$data['data'] = $licenses;
		$this->load->view('header');
		$this->load->view('licenses', $data);
	}

	public function form_add() {
		$this->load->model('customers_model');
		$customers = $this->customers_model->getall();
		$data['data']['customers'] = $customers;

		$this->load->model('hosts_model');
		$hosts = $this->hosts_model->getall();
		$data['data']['hosts'] = $hosts;

		$this->load->model('modules_model');
		$modules = $this->modules_model->getall();
		$data['data']['modules'] = $modules;
	
		$this->load->model('licenses_model');
		$licenses = $this->licenses_model->getall();
		$data['data']['licenses'] = $licenses;

		$this->load->view('header');
		$this->load->view('form/licenses_add',$data);
	}

	public function add() {
		$this->load->model('licenses_model');
		$return = $this->licenses_model->add($_POST);
		$this->load->view('header');
		print $return;
	}

	public function form_edit($id) {
		$this->load->model('customers_model');
		$customers = $this->customers_model->getall();
		$data['data']['customers'] = $customers;

		$this->load->model('hosts_model');
		$hosts = $this->hosts_model->getall();
		$data['data']['hosts'] = $hosts;

		$this->load->model('modules_model');
		$modules = $this->modules_model->getall();
		$data['data']['modules'] = $modules;
	
		$this->load->model('licenses_model');
		$licenses = $this->licenses_model->getid($id);
		$data['data']['licenses'] = $licenses;

		$this->load->view('header');
		$this->load->view('form/licenses_edit', $data);
	}

	public function edit() {
		$this->load->model('licenses_model');
		$return = $this->licenses_model->edit($_POST);
		$this->load->view('header');
		print $return;
	}

	public function del($id) {
		$this->load->model('licenses_model');
		$return = $this->licenses_model->del($id);
		$this->load->view('header');
		print $return;
	}

}