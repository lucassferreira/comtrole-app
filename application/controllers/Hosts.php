<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hosts extends CI_Controller {

	public function index(){
		$this->load->model('hosts_model');
		$hosts = $this->hosts_model->getall();
		$data['data'] = $hosts;
		$this->load->view('header');
		$this->load->view('hosts', $data);
	}

	public function show($id) {
		$this->load->model('hosts_model');
		$hosts = $this->hosts_model->getid($id);
		$data['data'] = $hosts;
		$this->load->view('header');
		$this->load->view('hosts', $data);
	}

	public function form_add() {
		$this->load->view('header');
		$this->load->view('form/hosts_add');
	}

	public function add() {
		$this->load->model('hosts_model');
		$return = $this->hosts_model->add($_POST);
		$this->load->view('header');
		print $return;
	}

	public function form_edit($id) {
		$this->load->model('hosts_model');
		$hosts = $this->hosts_model->getid($id);
		$data['data'] = $hosts;
		$this->load->view('header');
		$this->load->view('form/hosts_edit', $data);
	}

	public function edit() {
		$this->load->model('hosts_model');
		$return = $this->hosts_model->edit($_POST);
		$this->load->view('header');
		print $return;
	}

	public function del($id) {
		$this->load->model('hosts_model');
		$return = $this->hosts_model->del($id);
		$this->load->view('header');
		print $return;
	}

}
